//
//  ImageCaptureController.swift
//  NumberPlateReader
//
//  Created by Dipak Pandey on 26/07/19.
//  Copyright © 2019 Dipak Pandey. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire

class ImageCaptureController: UIViewController, AVCapturePhotoCaptureDelegate {

    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var imagePreview: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    
    var imageUploadUrl = ""
    var mobileNumber = ""
    var otpNumber = ""
    var showRectangle = true
    var captureSession: AVCaptureSession!
    var capturePhotoOutput: AVCapturePhotoOutput!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    var capturedImage: UIImage?
    var rectangleFrame = CGRect()


    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupCamera()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.captureSession.stopRunning()
        super.viewWillDisappear(animated)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func setupCamera() {
        captureSession = AVCaptureSession()
        //captureSession.sessionPreset = .medium
        
        guard let backCamera = AVCaptureDevice.default(for: AVMediaType.video)
            else {
                print("Unable to access back camera!")
                return
        }
        
        do {
            let input = try AVCaptureDeviceInput(device: backCamera)
            capturePhotoOutput = AVCapturePhotoOutput()
            
            if captureSession.canAddInput(input) && captureSession.canAddOutput(capturePhotoOutput) {
                captureSession.addInput(input)
                capturePhotoOutput.isHighResolutionCaptureEnabled = true
                captureSession.addOutput(capturePhotoOutput)
                setupLivePreview()
            }
        }
        catch let error  {
            print("Error Unable to initialize back camera:  \(error.localizedDescription)")
        }
        
        
    }
    
    func setupLivePreview() {
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        
        videoPreviewLayer.videoGravity = .resizeAspectFill
        videoPreviewLayer.connection?.videoOrientation = .portrait
        cameraView.layer.addSublayer(videoPreviewLayer)
        
        DispatchQueue.global(qos: .userInitiated).async { //[weak self] in
            self.captureSession.startRunning()
            
            DispatchQueue.main.async {
                self.cameraView.frame = self.view.bounds
                self.videoPreviewLayer.frame = self.view.bounds
                self.drawRectangleOnCamera()
            }
        }
        
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        guard let imageData = photo.fileDataRepresentation()
            else { return }
        
        if let image = UIImage(data: imageData) {
            
            let updatedImage = drawRectangleOnImage(originalImage: image)
            capturedImage = updatedImage
            imagePreview.image = capturedImage
          
        }
    }
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resetButtonAction(_ sender: Any) {
        imagePreview.image = nil
        capturedImage = nil
    }
    
    @IBAction func takePictureButtonAction(_ sender: Any) {
        let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
        capturePhotoOutput.capturePhoto(with: settings, delegate: self)
        
    }
   
    @IBAction func uploadImageAction(_ sender: Any) {
        
        uploadImage()
    }
    
    //MARK - API Calls
    
    func uploadImage() {
        
        guard let image = capturedImage
            else { return }
        
        showHUD()

        let scale =  UIScreen.main.scale
        var imageFrame = rectangleFrame
        if scale > 1.0 {
            imageFrame = CGRect.init(x: rectangleFrame.origin.x * scale, y: rectangleFrame.origin.y * scale, width: rectangleFrame.size.width * scale, height: rectangleFrame.size.height * scale)
        }

        let param: [String: Any] = ["rectangle_frame" : "\(imageFrame)", "mobile_number": mobileNumber, "otp" : otpNumber]
        
        //var imageUrl = "https://default2-dot-local-exp-1.appspot.com/reader"
        var imageUrl = "https://local-exp-1.appspot.com/reader"

        if !imageUploadUrl.isEmpty {
            imageUrl = imageUploadUrl
        }
        
        let imageData = image.jpegData(compressionQuality: 0.7)
        
        //let imageData = image.pngData()

        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(imageData!, withName: "img", fileName: "numberplate", mimeType: "image/jpeg")
            for (key, value) in param {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: imageUrl)
        { (result) in

            switch result {

            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("uploading \(progress)")
                    
                })
                
                upload.responseJSON { response in
                    self.dismissHUD()
                    let statusCode = response.response?.statusCode

                    var alertMessage = ""
                    if statusCode == 200 || statusCode == 201 {
                        alertMessage = "Image uploaded successfully"
                    } else {
                        alertMessage = "Failed to upload image. Status code:\(statusCode ?? 00)"
                    }
                    print("resopnse code is : \(String(describing: response.response?.statusCode))")
                    NSLog("response is : \(response)")
                    self.showAlert(message: alertMessage)

                }
            case .failure(let encodingError):
                print("the error is  : \(encodingError.localizedDescription)")
                self.showAlert(message: encodingError.localizedDescription)
                break
            }
        }
    }
    
    
    func showHUD(){
        
        DispatchQueue.main.async{
        
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
            self.view.isUserInteractionEnabled = false
        }
    }
    
    func dismissHUD() {

        DispatchQueue.main.async{
            self.view.isUserInteractionEnabled = true
            self.activityIndicator.isHidden = true
            self.activityIndicator.startAnimating()
        }
    }
    
    
    func removeRecatngleFromCamera() {
        
        if !showRectangle {
           let _ = cameraView.layer.sublayers?.popLast()
        }
        
    }
    
    
    func drawRectangleOnCamera() {
        if !showRectangle {
           return
        }
        
        let rectangleLayer = CAShapeLayer()
        rectangleLayer.path = UIBezierPath(rect: getRectangleFrame(forViewSize:videoPreviewLayer.frame.size)).cgPath
        
        rectangleLayer.strokeColor = UIColor.red.cgColor
        rectangleLayer.fillColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0).cgColor
        cameraView.layer.insertSublayer(rectangleLayer, above: videoPreviewLayer)

    }
    
    func drawRectangleOnImage(originalImage: UIImage) -> UIImage {
        
        if !showRectangle {
            return originalImage
        }
        rectangleFrame = getRectangleFrame(forViewSize:originalImage.size)
    
        UIGraphicsBeginImageContextWithOptions(originalImage.size, false, UIScreen.main.scale)
        
        originalImage.draw(at: CGPoint(x: 0, y: 0))
        let context = UIGraphicsGetCurrentContext()!
        
        context.addRect(rectangleFrame)
        context.setStrokeColor(UIColor.red.cgColor)
        context.strokePath()
        
        if let newImage = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            return newImage
        }
        return originalImage
        
    }

    //forViewSize: Size of videoPreviewLayer/Image
    func getRectangleFrame(forViewSize: CGSize) -> CGRect {
        
        let recatangleSize = UIDevice.current.userInterfaceIdiom == .pad ? CGSize.init(width: 600, height: 350) : CGSize.init(width: 300, height: 200)
        
        var recatangleFrame = CGRect.init(x: 100, y: 100, width: recatangleSize.width, height: recatangleSize.height)
        recatangleFrame.origin.x = (forViewSize.width - recatangleFrame.size.width) / 2
        recatangleFrame.origin.y = (forViewSize.height - recatangleFrame.size.height) / 2
        
        print("View Size:\(forViewSize)")
        print("Recatangle frame:\(recatangleFrame)")
        
        return recatangleFrame
    }
    
    func showAlert(message: String) {
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
}

