//
//  HomeVC.swift
//  NumberPlateReader
//
//  Created by Dipak Pandey on 01/08/19.
//  Copyright © 2019 Dipak Pandey. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    @IBOutlet weak var txfURL: UITextField!
    @IBOutlet weak var txfMobileNumber: UITextField!
    @IBOutlet weak var lblOTP: UILabel!
    @IBOutlet weak var boxSwitch: UISwitch!

    var imageUploadUrl = ""
    var mobileNumber = "+6591863850"

    
    override func viewDidLoad() {
        super.viewDidLoad()
        txfMobileNumber.text = mobileNumber
         generateOTP()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        hideKeybaord()
        super.viewWillDisappear(animated)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier == "ImageCaptureController") {
            let imageVC = segue.destination as! ImageCaptureController
            imageVC.showRectangle = boxSwitch.isOn
            imageVC.imageUploadUrl = imageUploadUrl
            imageVC.mobileNumber = mobileNumber
            imageVC.otpNumber = lblOTP!.text ?? ""
            
        }
    }
    
    func isValidURL(urlString: String?) -> Bool {

        if let urlString = urlString, !urlString.isEmpty {
            if let url = NSURL(string: urlString), let _ = url.scheme, let _ = url.host {
                return true
            }
        }
        return false
    }
 
    @IBAction func saveMobileNumber(_ sender: Any) {
        mobileNumber = txfMobileNumber!.text ?? "+6591863850"
        hideKeybaord()
    }
    
    @IBAction func genrateOTPAction(_ sender: Any) {
        generateOTP()
    }
    
    @IBAction func saveUrlAction(_ sender: Any) {
        
        hideKeybaord()
        
        if isValidURL(urlString: txfURL.text) {
            imageUploadUrl = txfURL.text ?? ""
        } else {
            showAlert()
        }
        
    }

    func generateOTP() {
        
        var result = ""
        repeat {
            // Create a string with a random number 0...9999
            result = String(format:"%04d", arc4random_uniform(10000) )
        } while result.count < 4
        lblOTP!.text = result
    }

    func hideKeybaord() {
        txfMobileNumber.resignFirstResponder()
        txfURL.resignFirstResponder()
        
    }
    
    func showAlert() {
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Alert", message: "Please enter valid url.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
}
